# postgres
create user repluser replication login encrypted password '12345' connection limit -1;
rm -rf /var/lib/pgsql/11/data/*
pg_basebackup -h 192.168.50.10 -U repluser -D /var/lib/pgsql/11/data/

```
postgres=# select * from pg_stat_replication;
-[ RECORD 1 ]----+------------------------------
pid              | 6199
usesysid         | 16384
usename          | repluser
application_name | walreceiver
client_addr      | 192.168.50.11
client_hostname  |
client_port      | 60704
backend_start    | 2019-08-24 14:40:13.634152+00
backend_xmin     |
state            | streaming
sent_lsn         | 0/3000140
write_lsn        | 0/3000140
flush_lsn        | 0/3000140
replay_lsn       | 0/3000140
write_lag        |
flush_lag        |
replay_lag       |
sync_priority    | 0
sync_state       | async
```
barman

```
[root@pgslave vagrant]# barman check pg
WARNING: No backup strategy set for server 'pg' (using default 'exclusive_backup').
WARNING: The default backup strategy will change to 'concurrent_backup' in the future. Explicitly set 'backup_options' to silence this warning.
Server pg:
	PostgreSQL: OK
	is_superuser: OK
	wal_level: OK
	directories: OK
	retention policy settings: OK
	backup maximum age: OK (no last_backup_maximum_age provided)
	compression settings: OK
	failed backups: OK (there are 0 failed backups)
	minimum redundancy requirements: OK (have 3 backups, expected at least 3)
	ssh: OK (PostgreSQL server)
	not in recovery: OK
	archive_mode: OK
	archive_command: OK
	continuous archiving: OK
	archiver errors: OK
[root@pgslave vagrant]#

```
